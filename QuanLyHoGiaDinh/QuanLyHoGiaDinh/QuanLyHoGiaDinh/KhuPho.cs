﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHoDanCu
{
    class KhuPho : HoGiaDinh
    {
        private HoGiaDinh[] dsHoGiaDinh_222 = new HoGiaDinh[10];
        private int soHoGiaDinh_222;

        public void InThongTinKhuPho()
        {
            Console.Write("Nhap so ho gia dinh: ");
            soHoGiaDinh_222 = int.Parse(Console.ReadLine());
            for (int i_222 = 1; i_222 <= soHoGiaDinh_222; i_222++)
            {
                Console.WriteLine("Nhap ho gia dinh thu " + i_222);
                dsHoGiaDinh_222[i_222] = new HoGiaDinh();
                dsHoGiaDinh_222[i_222].NhapHoGiaDinh();
            }
            Console.WriteLine("Thong tin tat ca ho gia dinh: ");
            for (int i_222 = 1; i_222 <= soHoGiaDinh_222; i_222++)
            {
                Console.WriteLine("Ho gia dinh thu " + i_222);
                dsHoGiaDinh_222[i_222].InThongTinHoGiaDinh();
            }
        }
    }
}