﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHoDanCu
{
    class Nguoi
    {
        private string hoTen_222;
        private int tuoi_222;
        private string ngheNghiep_222;
        private int cmnd_222;

        public Nguoi() { }

        public Nguoi(string hoTen_222, int tuoi_222, string ngheNghiep_222, int cmnd_222)
        {
            this.hoTen_222 = hoTen_222;
            this.tuoi_222 = tuoi_222;
            this.ngheNghiep_222 = ngheNghiep_222;
            this.cmnd_222 = cmnd_222;
        }

        public string HoTen_222 { get => hoTen_222; set => hoTen_222 = value; }
        public int Tuoi_222 { get => tuoi_222; set => tuoi_222 = value; }
        public string NgheNgiep_222 { get => ngheNghiep_222; set => ngheNghiep_222 = value; }
        public int Cmnd_222 { get => cmnd_222; set => cmnd_222 = value; }

        public void NhapThongTinNguoi()
        {
            Console.WriteLine("Nhap ho va ten : ");
            hoTen_222 = Console.ReadLine();
            Console.WriteLine("Nhap tuoi: ");
            tuoi_222 = int.Parse(Console.ReadLine());
            Console.WriteLine("Nhap nghe nghiep:  ");
            ngheNghiep_222 = Console.ReadLine();
            Console.WriteLine("Nhap so cmnd: ");
            cmnd_222 = int.Parse(Console.ReadLine());
        }

        public void InThongTinNguoi()
        {
            Console.WriteLine("Ho ten: {0}, Tuoi: {1}, Nghe nghiep: {2}, CMND: {3}", hoTen_222, tuoi_222, ngheNghiep_222, cmnd_222);
        }
    }
}