﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHoDanCu
{
    class HoGiaDinh : Nguoi
    {
        private int soThanhVien_222;
        private int soNha_222;
        private Nguoi[] tv_222 = new Nguoi[10];

        public int SoThanhVien_222 { get => soThanhVien_222; set => soThanhVien_222 = value; }
        public int SoNha_222 { get => soNha_222; set => soNha_222 = value; }
        internal Nguoi[] Tv_222 { get => tv_222; set => tv_222 = value; }

        public HoGiaDinh()
        {

        }

        public HoGiaDinh(int soThanhVien_222, int soNha_222, Nguoi[] tv_222)
        {
            this.soThanhVien_222 = soThanhVien_222;
            this.soNha_222 = soNha_222;
            this.tv_222 = tv_222;
        }

        public void NhapHoGiaDinh()
        {
            Console.Write("Nhap so thanh vien: ");
            soThanhVien_222 = int.Parse(Console.ReadLine());
            Console.Write("Nhap so nha: ");
            soNha_222 = int.Parse(Console.ReadLine());
            for (int i_222 = 1; i_222 <= soThanhVien_222; i_222++)
            {
                Console.WriteLine("Nhap so thanh vien thu " + i_222);
                tv_222[i_222] = new Nguoi();
                tv_222[i_222].NhapThongTinNguoi();
            }
        }
        public void InThongTinHoGiaDinh()
        {
            Console.WriteLine("So thanh vien: {0}, So nha: {1}", soThanhVien_222, soNha_222);
            for (int i_222 = 1; i_222 <= soThanhVien_222; i_222++)
            {
                Console.WriteLine("Thanh vien thu " + i_222);
                tv_222[i_222].InThongTinNguoi();
            }
        }
    }
}