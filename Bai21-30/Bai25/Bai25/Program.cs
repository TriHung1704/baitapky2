﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai25
{
    class Program
    {
            static int USCLN(int a222, int b222)
            {
                if (b222 == 0) return a222;
                return USCLN(b222, a222 % b222);
            }

            /**
             * Tim boi so chung nho nhat (BSCNN)
             */
            static int BSCNN(int a222, int b222)
            {
                return (a222 * b222) / USCLN(a222, b222);
            }

            /**
            * Ham main
            */
            static void Main(string[] args)
            {
                Console.Write("Nhap so nguyen duong a222 = ");
                String valA = Console.ReadLine();
                int a222 = Convert.ToInt32(valA);
                Console.Write("Nhap so nguyen duong b222 = ");
                String valB = Console.ReadLine();
                int b222 = Convert.ToInt32(valB);
                // tinh USCLN cua a222 và b222
                Console.Write("USCLN cua {0} va {1} la: {2} \n", a222, b222, USCLN(a222, b222));
                // tinh BSCNN cua a222 và b222
                Console.Write("USCLN cua cua {0} va {1} la: {2}", a222, b222, BSCNN(a222, b222));
                Console.ReadKey();
        }
    }
}
