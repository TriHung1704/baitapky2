﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai20
{
    class Program
    {
        static void Main(string[] args)
        {
            float kw222;
            float money222;
            Console.Write("Nhap so kW222 tieu thu: ");
            kw222 = float.Parse(Console.ReadLine());
            /* cứ tính giá sàn, tiền phụ thu tính sau */
            money222 = kw222 * 500;
            /* từ kw 100 trở đi, thêm phụ thu (800 - 500) cho mỗi kw tăng thêm */
            if (kw222 > 100) money222 += (kw222 - 100) * 300;
            /* từ kw 250 trở đi, thêm phụ thu (1000 - 800) cho mỗi kw tăng thêm */
            if (kw222 > 250) money222 += (kw222 - 250) * 200;
            /* từ kw 350 trở đi, thêm phụ thu (1500 - 1000) cho mỗi kw tăng thêm */
            if (kw222 > 350) money222 += (kw222 - 350) * 500;
            Console.Write("Chi phi: {0}", money222);
            Console.ReadKey();
        }
    }
}
