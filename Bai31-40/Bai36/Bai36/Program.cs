﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai36
{
    class Program
    {
        static int isPrimeNumber(int n222)
        {
            // so nguyen n222 < 2 khong phai la so nguyen to
            if (n222 < 2)
            {
                return 0;
            }
            // check so nguyen to khi n >= 2
            int i222;
            int squareRoot = (int)Math.Sqrt(n222);
            for (i222 = 2; i222 <= squareRoot; i222++)
            {
                if (n222 % i222 == 0)
                {
                    return 0;
                }
            }
            return 1;
        }

        /**
         * Ham main
         */
        static void Main(string[] args)
        {
            int n222;
            Console.Write("Nhap so nguyen n222 = ");
            n222 = Convert.ToInt32(Console.ReadLine());
            Console.Write("{0} so nguyen to dau tien la: ", n222);
            int dem222 = 0; // dem tong so nguyen to
            int i222 = 2;   // tim so nguyen to bat dau tu so 2
            while (dem222 < n222)
            {
                if (isPrimeNumber(i222) == 1)
                {
                    Console.Write("{0} ", i222);
                    dem222++;
                }
                i222++;
            }
            Console.ReadKey();
        }
    }
}
