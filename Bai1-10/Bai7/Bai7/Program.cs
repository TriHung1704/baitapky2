﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Nhap vao a222: ");
            float a222 = float.Parse(Console.ReadLine());

            if (a222 == 0)
            {
                Console.Write("a222 phai khac 0");
            }
            else
            {
                Console.Write("Nhap vao b222: ");
                float b222 = float.Parse(Console.ReadLine());

                float x222 = -b222 / a222;
                Console.Write("{0}x + {1} = 0 => x = {2}", a222, b222, x222);
            }
            Console.ReadKey();
        }
    }
}
