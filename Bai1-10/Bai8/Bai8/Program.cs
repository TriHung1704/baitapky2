﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai8
{
    class Program
    {
        static void Main(string[] args)
        {
            float a222, b222, c222;
            Console.Write("Nhap vao a222 : ");
            a222 = float.Parse(Console.ReadLine());
            Console.Write("Nhap vao b222 : ");
            b222 = float.Parse(Console.ReadLine());
            Console.Write("Nhap vao c222 : ");
            c222 = float.Parse(Console.ReadLine());
            float delta = b222 * b222 - 4 * a222 * c222;
            if (delta > 0)
            {
                Console.Write("Phuong trinh co hai nghiem : ");
                Console.WriteLine("X1 = {0}", ((-b222 - Math.Sqrt(delta)) / 2 * a222));
                Console.Write("X2 = {0}", ((-b222 + Math.Sqrt(delta)) / 2 * a222));

            }
            else if (delta == 0)
            {
                Console.Write("Phuong trinh co hai nghiem kep nghiem");
                Console.Write("X1 = X2 {0}", -b222 / 2 * a222);
            }
            else if (delta < 0)
            {
                Console.Write("Phuong trinh vo nghiem");
            }
            Console.ReadKey();
        }
    }
}
